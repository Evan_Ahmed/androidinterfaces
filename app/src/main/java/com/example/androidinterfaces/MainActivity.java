package com.example.androidinterfaces;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.FrameLayout;

import PaymentMethod.PaymentMethodFragment;

public class MainActivity extends AppCompatActivity {

    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frameLayout = findViewById(R.id.container);
        PaymentMethodFragment goToFragment = new PaymentMethodFragment();
        getSupportFragmentManager().beginTransaction()
                .add(frameLayout.getId() , goToFragment)
                .commit();
    }
}
