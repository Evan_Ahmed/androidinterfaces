package PaymentMethod;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidinterfaces.R;

import java.util.List;

public class PaymentMethodAdapter extends RecyclerView.Adapter <PaymentMethodAdapter.MyViewHolder>{

    private List<PaymentMethodModel>paymentMethodList;



    public PaymentMethodAdapter(List<PaymentMethodModel> paymentMethodList) {
        this.paymentMethodList = paymentMethodList;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder{

        public ImageView iconImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            iconImage=itemView.findViewById(R.id.iconImage);
        }
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_method_recycler_list, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        PaymentMethodModel paymentMethodModel = paymentMethodList.get(position);
        holder.iconImage.setImageResource(paymentMethodModel.getIconImage());

    }


    @Override
    public int getItemCount() {
        return paymentMethodList.size();
    }


}
