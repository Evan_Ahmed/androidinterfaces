package PaymentMethod;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidinterfaces.R;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethodFragment extends Fragment {

    private View view;
    private List<PaymentMethodModel> paymentMethodList = new ArrayList<>();
    RecyclerView paymentMethodRecyclerView;
    PaymentMethodAdapter paymentMethodAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_payment_method, container, false);

        initViews();
        loadContents();
        initListeners();
        paymentMethodData();

        return view;
    }

    private void initViews() {

        this.paymentMethodRecyclerView = view.findViewById(R.id.paymentMethodRecyclerView);
        this.paymentMethodAdapter = new PaymentMethodAdapter(paymentMethodList);
        RecyclerView.ItemDecoration itemDecoration = new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(10, 0 , 10 , 0);
            }
        };

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext() , RecyclerView.HORIZONTAL , false);
        paymentMethodRecyclerView.setLayoutManager(mLayoutManager);
        paymentMethodRecyclerView.setItemAnimator(new DefaultItemAnimator());
        paymentMethodRecyclerView.setAdapter(paymentMethodAdapter);
        paymentMethodRecyclerView.addItemDecoration(itemDecoration);
    }

    private void loadContents() {
    }

    private void initListeners() {
    }


    private void paymentMethodData() {
        PaymentMethodModel paymentMethod = new PaymentMethodModel(R.drawable.ic_visa);
        paymentMethodList.add(paymentMethod);

        paymentMethod = new PaymentMethodModel(R.drawable.ic_mastercard);
        paymentMethodList.add(paymentMethod);

        paymentMethod = new PaymentMethodModel(R.drawable.ic_jcb);
        paymentMethodList.add(paymentMethod);

        paymentMethod = new PaymentMethodModel(R.drawable.ic_american_express);
        paymentMethodList.add(paymentMethod);

        paymentMethod = new PaymentMethodModel(R.drawable.ic_hsbc);
        paymentMethodList.add(paymentMethod);

    }
}
