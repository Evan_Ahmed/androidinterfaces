package PaymentMethod;

public class PaymentMethodModel {
    int iconImage;

    public int getIconImage() {
        return iconImage;
    }

    public void setIconImage(int iconImage) {
        this.iconImage = iconImage;
    }

    public PaymentMethodModel(int iconImage) {
        this.iconImage = iconImage;
    }
}
